program Tanki;

uses
  menu, graphics, game;

var
  wdth := 1104;
  hght := 897;
  size := 69;
  resources := 'High/';

begin
  
  if (SplashDraw(wdth, hght, resources)) then
    if (ResourcesLoad(resources, size)) then
      if (Splash2Draw(wdth, hght, resources)) then
      begin
        CreateMap;
        Sleep(1000);
        GameLogic;
      end;
end.