unit game;

interface

procedure GameLogic;

implementation

uses
  graphics, bots;

procedure GameLogic;
begin
  repeat
    Clear;   
    TankDraw;
    Shot;
    BotsLogic;
    DrawUI;  
    if (score = 550) then
    begin
      NextLevel;
    end;
    sleep(10);
  until false;
end;

begin

end. 