unit Graphics;

interface

const
  speed = 4;
  bullet_speed = 10;
  Count_lvls = 2;

var
  up, right, down, left, ctrl, Insert: boolean; //��������� ������
  k: byte := 1; //�����������
  l: byte := 1;//����������� ���������� �����
  Map: array[1..Count_lvls, 1..13, 1..13] of integer;// ������ � ������������� ������ �� ������
  level := 1; //������� �������
  alive: boolean = true;
  score: integer = 0;
  lives: integer = 3;
  size := 69;
  WinWidth, WinHeight: integer;
  LoadSound := new System.Media.soundPlayer('resources/sound.wav'); //���� ��� ��������
  ShotSound := new System.Media.soundPlayer('resources/shot.wav');//���� ��������

///������� �������� ��������
function ResourcesLoad(resources: string; size: integer): boolean;
///��������� ����� ������
procedure TankDraw;
///��������� ����� �����
procedure Enemydraw(x, y: integer);
///������� �������
procedure Clear;
///��������� ��������(��������) �����
procedure CreateMap;
///��������� ��������� ����������
procedure DrawUI;
///��������� ��������� ��������
procedure Shot;
///������� ��������� �������� (������)
function CollUp: boolean;
///������� ��������� �������� (�����)
function CollDown: boolean;
///������� ��������� �������� (�����)
function CollLeft: boolean;
///������� ��������� �������� (������)
function Collright: boolean;
procedure ScoreDraw;
procedure NextLevel;
procedure Respawn;

implementation

uses
  GraphABC;

type
  Tank = record
    pic: array[1..4] of Picture;
    alive: boolean;
    x, y: integer;
    dx, dy: integer;
    size := 69;
  end;

var
  Player, Enemy: Tank; 
  Bullet, Block: array[1..4] of Picture;
  LeftBar, Boom: Picture;
  Number: array[0..9] of Picture;
  _Score, _Lives, _Level: Picture;

function ResourcesLoad(resources: string; size: integer): boolean;
begin
  result := true;
  try
    for var i := 1 to 4 do //������ ��� ���� ������, ������� � �����  
    begin
      Player.pic[i] := Picture.Create(player.size, player.size);
      Player.pic[i].Load('resources/' + resources + 'Tank' + IntToStr(i) + '.bmp');
      Player.pic[i].Transparent := true;
      Player.pic[i].TransparentColor := clBlack;
      Bullet[i] := Picture.Create(5, 10);
      Bullet[i].Load('resources/' + resources + 'Bullet' + IntToStr(i) + '.bmp');
      Bullet[i].Transparent := true;
      Bullet[i].TransparentColor := clBlack;
      Block[i] := Picture.Create(size, size);
      Block[i].Load('resources/' + resources + 'Block' + IntToStr(i) + '.bmp');
      Block[i].Transparent := true;
      Block[i].TransparentColor := clBlack;
      Enemy.pic[i] := Picture.Create(player.size, player.size);
      Enemy.pic[i].Load('resources/' + resources + 'Enemy' + IntToStr(i) + '.bmp');
      Enemy.pic[i].Transparent := true;
      Enemy.pic[i].TransparentColor := clBlack;
      Font.Color := rgb(0, 128, 128);
    end;
    for var i := 0 to 9 do
    begin
      Number[i] := Picture.Create(34, 56);
      Number[i].Load('resources/' + resources + IntToStr(i) + '.bmp');
      Number[i].Transparent := true;
      Number[i].TransparentColor := clWhite;
    end;  
    _Score := Picture.Create(100, 56);
    _Score.Load('resources/' + resources + 'score.bmp');
    _Score.Transparent := true;
    _Score.TransparentColor := clWhite;
    _Lives := Picture.Create(100, 56);
    _Lives.Load('resources/' + resources + 'lives.bmp');
    _Lives.Transparent := true;
    _Lives.TransparentColor := clWhite;
    _Level := Picture.Create(100, 56);
    _Level.Load('resources/' + resources + 'level.bmp');
    _Level.Transparent := true;
    _Level.TransparentColor := clWhite;
    LeftBar := Picture.Create(432, 1180);
    LeftBar.Load('resources/' + resources + 'LeftBar.bmp');
    Boom := Picture.Create(size, size);
    Boom.Load('resources/' + resources + 'Boom.bmp');
    Boom.Transparent := true;
    Boom.TransparentColor := clBlack;
    TextOut(10, 30, '�������� �������� - OK!');
  except
    result := false;
    Font.Color := rgb(255, 0, 0);
    TextOut(10, 30, '�������� �������� - ERROR!');
    TextOut(10, 90, '�����...');
    Sleep(10000);
    Halt;
  end;
end;

///��������� ��������� �������
procedure KeyDown(key: integer);
begin
  case Key of
    VK_Escape: Halt;
    VK_Up: 
      begin
        if (right = false) and (left = false) and (down = false) then
          k := 1;  
        up := true;
      end;
    VK_Right:     
      begin
        if (up = false) and (left = false) and (down = false) then
          k := 4;
        right := true;
      end;
    VK_Down: 
      begin
        if (left = false) and (right = false) and (up = false) then
          k := 3;
        down := true;
      end;
    VK_Left: 
      begin
        if (up = false) and (down = false) and (right = false) then
          k := 2;
        left := true;
      end;
    VK_ControlKey: ctrl := true;
    VK_Insert: 
      begin
        if (Insert = true) then
          Insert := false
        else Insert := true;
      end;  
    VK_R:
      begin
        Respawn;
      end;
  end;
end;

///��������� ��������� �������
procedure KeyUp(Key: integer);
begin
  case Key of
    VK_Up: 
      begin
        up := false;
        Player.dx := 0;
        Player.dy := 0;
      end;
    VK_Right: 
      begin
        right := false;
        Player.dx := 0;
        Player.dy := 0;
      end;  
    VK_Down: 
      begin
        down := false;
        Player.dy := 0;
        Player.dx := 0;
      end;
    VK_Left: 
      begin
        left := false;
        Player.dx := 0;
        Player.dy := 0;
      end;
    VK_ControlKey: ctrl := false;
  end;
end;

procedure TankDraw;
begin
  if (up) and (CollUp = false) and (right = false) and (left = false) and (down = false) then
  begin
    Player.dx := 0;
    if (Player.y <= 2) then // ������
      Player.dy := 0
    else  
      Player.dy := -speed;
  end;
  if (right) and (collRight = false) and (up = false) and (left = false) and (down = false) then
  begin
    Player.dy := 0;
    if (Player.x >= WindowWidth - 275) then //������
      Player.dx := 0
    else
      Player.dx := +speed;
  end;
  if (down) and (CollDown = false) and (left = false) and (right = false) and (up = false) then
  begin
    Player.dx := 0;
    if (Player.y >= WindowHeight - 65) then //�����
      Player.dy := 0
    else
      Player.dy := +speed;
  end;   
  if (left) and (CollLeft = false) and (up = false) and (down = false) and (right = false) then
  begin
    Player.dy := 0;
    if (Player.x <= 3) then //�����
      Player.dx := 0
    else 
      Player.dx := -speed;
  end;
  
  Inc(Player.x, Player.dx);
  Inc(Player.y, Player.dy);
  if (left) or (right) then
    player.dx := 0
  else if (down) or (up) then
    player.dy := 0;
  Player.pic[k].draw(Player.x, Player.y);
end;

procedure EnemyDraw(x, y: integer);
begin
  Enemy.x := x;
  Enemy.y := y;
  if (alive) then
    Enemy.pic[l].draw(x, y);
end;

procedure Clear;
begin
  ClearWindow(rgb(0, 0, 0));
end;

procedure shot;
var
  bx, by: integer;
  kx, ky: integer;
begin
  if (ctrl) then  //��������
  begin
    ShotSound.Play;
    case k of
      1:                         // �����
        begin
          bx := Player.x + ((player.size - 5) div 2);
          by := Player.y + 20;
          Bullet[1].Draw(bx, by);
          repeat
            kx := (bx div size) + 1;
            ky := (by div size) + 1;
            if (ky <= 1) then	
              kx := 1
         			else
            begin
              if (map[level, kx, (ky - 1)] = 1) then
                if ((by - bullet_speed) <= ((ky - 1) * size)) then
                begin
                  map[level, kx, (ky - 1)] -= 1;
                  score += 10;
                  exit;
                end;
              if (map[level, kx, (ky - 1)] = 2) then
                if ((by - bullet_speed) <= ((ky - 1) * size)) then
                  exit;
              case l of
                1:
                  if (((by - bullet_speed) <= (Enemy.y + player.size)) and ((bx >= Enemy.x) and (bx <= (Enemy.x + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                2:
                  if (((by - bullet_speed) <= (Enemy.y + player.size)) and ((bx >= Enemy.x) and (bx <= (Enemy.x + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                3:
                  if (((by - bullet_speed) <= (Enemy.y + player.size)) and ((bx >= Enemy.x) and (bx <= (Enemy.x + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                4:
                  if (((by - bullet_speed) <= (Enemy.y + player.size)) and ((bx >= Enemy.x) and (bx <= (Enemy.x + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
              end;  
              
              //if ((by) <= (Enemy.x))
            end;
            by := by - bullet_speed;
            Clear;
            Bullet[1].Draw(bx, by);
            EnemyDraw(Enemy.x, Enemy.y);
            TankDraw;
            DrawUI;
            if (Insert) then
              TankDraw;
          until (by < 0);
        end;
      2:                         // �����
        begin
          bx := Player.x + 20;
          by := Player.y + ((player.size - 5) div 2);
          Bullet[2].Draw(bx, by);
          repeat
            kx := (bx div size) + 1;
            ky := (by div size) + 1; 
            if (kx <= 1) then
              kx := 1
            else
            begin
              if (map[level, (kx - 1), ky] = 1) then
                if ((bx - bullet_speed) <= ((kx - 1) * size)) then
                begin
                  map[level, (kx - 1), ky] -= 1;
                  score += 10;
                  exit;
                end;
              if (map[level, (kx - 1), ky] = 2) then
                if ((bx - bullet_speed) <= ((kx - 1) * size)) then
                  exit;
              case l of
                1:
                  if (((bx - bullet_speed) <= (Enemy.x + player.size)) and ((by >= Enemy.y) and (by <= (Enemy.y + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                2:
                  if (((bx - bullet_speed) <= (Enemy.x + player.size)) and ((by >= Enemy.y) and (by <= (Enemy.y + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                3:
                  if (((bx - bullet_speed) <= (Enemy.x + player.size)) and ((by >= Enemy.y) and (by <= (Enemy.y + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                4:
                  if (((bx - bullet_speed) <= (Enemy.x + player.size)) and ((by >= Enemy.y) and (by <= (Enemy.y + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    exit;
                  end;
              end;  
              
            end;
            bx := bx - bullet_speed;
            Clear;
            Bullet[2].Draw(bx, by);
            TankDraw;
            EnemyDraw(Enemy.x, Enemy.y);
            DrawUI;
            if (Insert) then
              TankDraw;
          until (bx < 0);
        end; 
      3:                         // ����
        begin
          bx := Player.x + ((player.size - 5) div 2);
          by := Player.y + player.size - 20;
          Bullet[3].Draw(bx, by);
          repeat
            kx := (bx div size) + 1;
            ky := (by div size) + 1; 
            if (ky >= 13) then	
              kx := 13
         			else
            begin
              if (map[level, kx, (ky + 1)] = 1) then
                if ((by + bullet_speed) <= ((ky + 1) * size)) then
                begin
                  map[level, kx, (ky + 1)] -= 1;
                  score += 10;
                  exit;
                end;
              if (map[level, kx, (ky + 1)] = 2) then
                if ((by + bullet_speed) <= ((ky + 1) * size)) then
                  exit;
              case l of
                1:
                  if (((by + bullet_speed) <= (Enemy.y)) and ((bx >= Enemy.x) and (bx <= (Enemy.x + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                2:
                  if (((by + bullet_speed) <= (Enemy.y)) and ((bx >= Enemy.x) and (bx <= (Enemy.x + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                3:
                  if (((by + bullet_speed) <= (Enemy.y)) and ((bx >= Enemy.x) and (bx <= (Enemy.x + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                4:
                  if (((by + bullet_speed) <= (Enemy.y)) and ((bx >= Enemy.x) and (bx <= (Enemy.x + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
              end;  
              
            end;
            by := by + bullet_speed;
            Clear;
            Bullet[3].Draw(bx, by);
            TankDraw;
            EnemyDraw(Enemy.x, Enemy.y);
            DrawUI;
            if (Insert) then
              TankDraw;
          until (by > WindowHeight);
        end;
      4:                         // ������
        begin
          bx := Player.x + player.size - 20;
          by := Player.y + ((player.size - 5) div 2);
          Bullet[4].Draw(bx, by);
          repeat
            kx := (bx div size) + 1;
            ky := (by div size) + 1;
            if (kx >= 13) then
              kx := 13
            else
            begin
              if (map[level, (kx + 1), ky] = 1) then
                if ((bx + bullet_speed) <= ((kx + 1) * size)) then
                begin
                  map[level, (kx + 1), ky] -= 1;
                  score += 10;
                  exit;
                end;
              if (map[level, (kx + 1), ky] = 2) then
                if ((bx + bullet_speed) <= ((kx + 1) * size)) then
                  exit;
              case l of
                1:
                  if (((bx + bullet_speed) <= (Enemy.x)) and ((by >= Enemy.y) and (by <= (Enemy.y + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                2:
                  if (((bx + bullet_speed) <= (Enemy.x)) and ((by >= Enemy.y) and (by <= (Enemy.y + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                3:
                  if (((bx + bullet_speed) <= (Enemy.x)) and ((by >= Enemy.y) and (by <= (Enemy.y + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
                4:
                  if (((bx + bullet_speed) <= (Enemy.x)) and ((by >= Enemy.y) and (by <= (Enemy.y + Player.size)))) then
                  begin
                    score += 100;
                    Boom.Draw(enemy.x, enemy.y);
                    alive := false;
                    exit;
                  end;
              end;  
              
            end;
            bx := bx + bullet_speed;
            Clear;
            Bullet[4].Draw(bx, by);
            TankDraw;
            EnemyDraw(Enemy.x, Enemy.y);
            DrawUI;
            if (Insert) then
              TankDraw;
          until (bx > 950);
        end;
    end;
  end;
end;

procedure CreateMap;
var
  levels: text;// ���������� ��� ����� � ��������
  s: string;
begin
  try
    assign(levels, 'resources/levels.txt'); //�������� �������
    reset(levels);
    for var n := 1 to Count_lvls do
    begin
      for var i := 1 to 13 do
      begin
        readln(levels, s);
        for var j := 1 to 13 do
        begin
          Map[n, j, i] := StrToInt(s[j]);
        end;
      end;
      readln(levels, s);
    end;
    close(levels);
    Font.Color := rgb(0, 128, 128);
    TextOut(10, 70, '�������� ������� - OK!');
  except
    Font.Color := rgb(255, 0, 0);
    TextOut(10, 70, '�������� ������� - ERROR!');
    TextOut(10, 90, '�����...');
    Sleep(10000);
    Halt;
  end;
end;

procedure DrawUI;
begin
  LockGraphics;
  LockDrawing;
  
  LeftBar.Draw(896, 0); 
  ScoreDraw;
  
  for var i := 1 to 13 do
    for var j := 1 to 13 do
      if Map[level, i, j] > 0 then
        Block[Map[level, i, j]].draw((i - 1) * size, (j - 1) * size);
  Redraw;
end;

function CollLeft: boolean;
var
  kx, ky: integer;
begin
  CollLeft := false;
  kx := (player.x div size) + 1; 
  ky := (player.y div size) + 1;
  if (kx <= 1) then
    kx := 2;
  if (map[level, (kx - 1), ky] = 1) or (map[level, (kx - 1), ky] = 2) then
    if ((player.x - speed) <= ((kx - 1) * size)) then
      CollLeft := true;
  if ((player.y + player.size) > 13 * size) then
    ky := 12
  else  
    ky := ((player.y + player.size - 5) div size) + 1;
  if (ky >= 13) then
    ky := 13;
  if (map[level, (kx - 1), ky] = 1) or (map[level, (kx - 1), ky] = 2) then
    if ((player.x - speed) <= ((kx - 1) * size)) then
      CollLeft := true; 
end;

function CollRight: boolean;
var
  kx, ky: integer;
begin
  CollRight := false;
  kx := (player.x div size) + 1; 
  ky := (player.y div size) + 1;
  if (kx >= 13) then
    kx := 13
  else
  begin
    if (map[level, (kx + 1), ky] = 1) or (map[level, (kx + 1), ky] = 2) then
      if ((player.x + speed) <= ((kx + 1) * size)) then
        CollRight := true;
    if ((player.y + player.size) >= 13 * size) then
      ky := 13
    else 
      ky := ((player.y + player.size - 5) div size) + 1;
    if (ky >= 13) then
      ky := 13;
    if (map[level, (kx + 1), ky] = 1) or (map[level, (kx + 1), ky] = 2) then
      if ((player.x + speed) <= ((kx + 1) * size)) then
        CollRight := true; 
  end;  
end;

function CollUp: boolean;
var
  kx, ky: integer;
begin
  CollUp := false;
  kx := (player.x div size) + 1; 
  ky := (player.y div size) + 1;
  if (ky = 1) then
    Exit
  else
  begin
    if (map[level, kx, (ky - 1)] = 1) or (map[level, kx, (ky - 1)] = 2) then
      if ((player.y - speed) <= ((ky - 1) * size)) then
        CollUp := true;
    if ((player.x + player.size) >= 13 * size) then
      kx := 13
    else 
      kx := ((player.x + player.size - 5) div size) + 1;
    if (map[level, kx, (ky - 1)] = 1) or (map[level, kx, (ky - 1)] = 2) then
      if ((player.y - speed) <= ((ky - 1) * size)) then
        CollUp := true; 
  end;
end;

function CollDown: boolean;
var
  kx, ky: integer;
begin
  CollDown := false;
  kx := (player.x div size) + 1; 
  ky := (player.y div size) + 1;
  if (ky = 13) then
    Exit
  else
  begin
    if (map[level, kx, (ky + 1)] = 1) or (map[level, kx, (ky + 1)] = 2) then
      if ((player.y + speed) <= ((ky + 1) * size)) then
        CollDown := true;
    if ((player.x + player.size) >= 13 * size) then
      kx := 13
    else 
      kx := ((player.x + player.size - 5) div size) + 1;
    if (map[level, kx, (ky + 1)] = 1) or (map[level, kx, (ky + 1)] = 2) then
      if ((player.y + speed) <= ((ky + 1) * size)) then
        CollDown := true; 
  end;
end;

procedure ScoreDraw;
var
  s1, s2, s3, s4: integer;
begin
  if (score = 100) then lives := 2;
  if (score = 200) then lives := 1;
  _Score.Draw(920, 100);
  s1 := score div 1000;
  s2 := (score - s1 * 1000) div 100;
  s3 := ((score - s1 * 1000) - (s2 * 100)) div 10;
  s4 := ((score - s1 * 1000) - (s2 * 100) - (s3 * 10));
  Number[s1].Draw(920, 170); 
  Number[s2].Draw(960, 170);
  Number[s3].Draw(1000, 170);
  Number[s4].Draw(1040, 170);
  _Lives.Draw(920, 240);
  Number[lives].Draw(920, 310);  
  _Level.Draw(920, 380);
  Number[0].Draw(920, 450); 
  Number[level].Draw(960, 450);
end;

procedure NextLevel;
begin
  player.dx := 0;
  player.dy := 0;
  
  player.x := 552;
  player.y := 828;
  k := 1;
  level := 2;
  alive := true;
end;

procedure Respawn;
begin
  enemy.x := 414;
  enemy.y := 0;
  alive := true;
end;

begin
  player.x := 552;
  player.y := 828;
  alive := true;
  
  WinHeight := WindowHeight;
  WinWidth := WindowWidth;
  Insert := false;
  LoadSound.Play;
  OnKeyDown := KeyDown;
  OnKeyUp := KeyUp;
  SetWindowIsFixedSize(true);
end.