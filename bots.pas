unit bots;

interface

procedure BotsLogic;

implementation

uses
  graphics;

type
  Tank = record
    x, y: integer;
    dx, dy: integer;
    size := 65;
  end;

const
  speed = 3;

var
  Enemy: Tank;
  counter: integer;
  movement, side: byte;
  schmove: byte = 0;
  botx, boty: integer;
  first: boolean = true;


procedure BotsLogic;
begin
  if (first = true) then 
  begin
    side := random(1, 4); //1-������ 2-������� 3-����� 4-����
    schmove := 0;
    first := false;
    
  end;
  
  if (side = 1) and (schmove >= 0) then //����� �������
  begin
    inc(schmove);
    l := 2;
    botx := (enemy.x div size) - 1;
    boty := (enemy.y div size) + 1;
    if (botx < 0) then 
      botx := 0;
    if ((enemy.x - 69) >= (size * botx)) and ((map[level, botx + 2, boty] = 0) or (map[level, botx + 2, boty] = 3)) and ((enemy.x - 69) >= 0) then
    begin
      inc(Enemy.x, -speed);
      EnemyDraw(Enemy.x, Enemy.y); 
      if schmove = 23 then
      begin
        schmove := 0;
        EnemyDraw(Enemy.x, Enemy.y);
        first := true;      
      end;
    end
    else
      first := true;   
    
  end;
  
  if (side = 2) and (schmove >= 0) then // ������ �������
  begin
    inc(schmove);
    l := 4;
    botx := (enemy.x div size) + 2;
    boty := (enemy.y div size) + 1;
    if (botx > 13) then 
      botx := 13;
    if ((enemy.x + 69 ) <= (size * botx)) and ((map[level, botx, boty] = 0) or (map[level, botx, boty] = 3)) and ((enemy.x + 69) <= 897) then
    begin
      inc(Enemy.x, speed);
      EnemyDraw(Enemy.x, Enemy.y); 
      if schmove = 23 then
      begin
        schmove := 0;
        EnemyDraw(Enemy.x, Enemy.y);
        first := true;      
      end;
    end
    else
      first := true; 
    
  end;
  
  if (side = 3) and (schmove >= 0) then //�����
  begin
    inc(schmove);
    l := 1;
    botx := (enemy.x div size);
    boty := (enemy.y div size);
    if (boty < 0) then 
      boty := 0;
    if ((enemy.y - 69) >= (size * (boty - 1))) and ((map[level, botx + 1, boty + 1] = 0) or (map[level, botx + 1, boty + 1] = 3)) and ((enemy.y - 69) >= 0) then
    begin
      inc(Enemy.y, -speed);
      EnemyDraw(Enemy.x, Enemy.y); 
      if schmove = 23 then
      begin
        schmove := 0;
        EnemyDraw(Enemy.x, Enemy.y);
        first := true;      
      end;
    end
    else
      first := true;   
    
  end;
  
  if (side = 4) and (schmove >= 0) then //����
  begin
    l := 3;
    botx := (enemy.x div size);
    boty := (enemy.y div size) + 2;
    if (boty >= 13) then
      boty := 13;
    if ((enemy.y + 3) <= (size * (boty - 1))) and ((map[level, botx + 1, boty] = 0) or (map[level, botx + 1, boty] = 3)) and ((enemy.y + 3) <= 897) then
    begin
      inc(Enemy.y, +speed);
      EnemyDraw(Enemy.x, Enemy.y); 
      if schmove = 23 then
      begin
        schmove := 0;
        EnemyDraw(Enemy.x, Enemy.y);
        first := true;      
      end;
    end
    else
      first := true;   
    
  end;
  
  EnemyDraw(Enemy.x, Enemy.y); //��������� �����
end;


begin
  
  enemy.x := 414;//��������� ��������� �������
  enemy.y := 0;

end. 