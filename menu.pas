unit Menu;

interface

function SplashDraw(wdth, hght: integer; resources: string): boolean;
function Splash2Draw(wdth, hght: integer; resources: string): boolean;

implementation

uses
  graphABC, graphics;

var
  Splash: picture; //����������� ��������
  resources: string;
  wdth, hght: integer;

///������� ����������� ����������� ��������
function SplashDraw(wdth, hght: integer; resources: string): boolean;
begin
  SetWindowSize(wdth, hght);
  CenterWindow; 
  SetWindowCaption('BattleCity v0.9.0');
  result := true;
  try
    ClearWindow(rgb(0, 0, 0));
    Splash := Picture.Create(wdth, hght); //���������� ����������� ��������
    Splash.Load('resources/' + resources + 'Splash.bmp');
    Splash.Draw(0, 0);
    Font.Color := rgb(0, 128, 128);
    TextOut(10, 10, '�������� ����� - OK!');
  except
    result := false;
    Font.Color := rgb(255, 0, 0);
    TextOut(10, 10, '�������� ����� - ERROR!');
    TextOut(10, 90, '�����...');
    Sleep(10000);
    Halt;
  end;
end;

///������� ����������� ����������� �������� �2
function Splash2Draw(wdth, hght: integer; resources: string): boolean;
begin
  result := true;
  try
    Splash.Load('resources/' + resources + 'Splash1.bmp');
    Splash.TransparentColor := clBlack;
    Splash.Transparent := true;    
    Splash.Draw(0, 0); //200, 650
    Font.Color := rgb(0, 128, 128);
    TextOut(10, 50, '�������� - OK!');
  except
    result := false;
    Font.Color := rgb(255, 0, 0);
    TextOut(10, 50, '�������� - ERROR!');
    TextOut(10, 90, '�����...');
    Sleep(10000);
    Halt;
  end;
end;

begin
end. 